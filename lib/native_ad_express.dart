import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tencent_ad/o.dart';

/// 原生模板广告
class NativeADExpress extends StatefulWidget {
  const NativeADExpress({
    Key key,
    this.posID,
    this.adCount: 5,
    this.callback,
    this.adWidth,
    this.adHeight,
    this.adIndex: 0,
  }) : super(key: key);

  final String posID;
  final int adCount; // 默认请求次数: 5
  final int adIndex; // 广告集合索引: 0
  final double adWidth;
  final double adHeight;
  final NativeADEventCallback callback;

  @override
  NativeADExpressState createState() => NativeADExpressState();
}

class NativeADExpressState extends State<NativeADExpress> {
  MethodChannel _channel;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.adWidth ?? double.infinity,
      height: widget.adHeight,
      child: defaultTargetPlatform == TargetPlatform.android
          ? AndroidView(
              viewType: '$nativeExpressID',
              onPlatformViewCreated: _onPlatformViewCreated,
              creationParams: {
                'posID': widget.posID,
                'adCount': widget.adCount,
                'adIndex': widget.adIndex,
              },
              creationParamsCodec: const StandardMessageCodec(),
            )
          : UiKitView(
              viewType: '$nativeExpressID',
              onPlatformViewCreated: _onPlatformViewCreated,
              creationParams: {
                'posID': widget.posID,
                'adCount': widget.adCount,
                'adIndex': widget.adIndex,
              },
              creationParamsCodec: StandardMessageCodec(),
            ),
    );
  }

  void _onPlatformViewCreated(int id) {
    print('the view is $nativeExpressID\_$id');
    _channel = MethodChannel('$nativeExpressID\_$id');
    _channel.setMethodCallHandler(_handleMethodCall);
    loadAD();
  }

  Future<void> _handleMethodCall(MethodCall call) async {
    NativeADEvent event;
    switch (call.method) {
      case 'onNoAD':
        event = NativeADEvent.onNoAD;
        String returnMesage = await onNoAD();
        call.arguments['resultMsg'] = returnMesage;
        break;
      case 'onLoadSuccess':
        event = NativeADEvent.onLoadSuccess;
        break;
      case 'onClick':
        event = NativeADEvent.onClick;
        break;
      case 'onExposed':
        event = NativeADEvent.onExposed;
        break;
      case 'onRenderFail':
        event = NativeADEvent.onExposed;
        break;
      case 'onRenderSuccess':
        event = NativeADEvent.onExposed;
        break;
      case 'onAdClosed':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoCache':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoStart':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoResume':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoPause':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoComplete':
        event = NativeADEvent.onExposed;
        break;
      case 'onVideoError':
        event = NativeADEvent.onExposed;
        break;
    }

    widget.callback(event, call.arguments);
  }

  Future<void> loadAD() async {
    await _channel.invokeMethod('loadAD', {
      'adCount': widget.adCount,
    });
  }

  Future<void> closeAD() async => await _channel.invokeMethod('closeAD');

  Future<String> onNoAD() async => await _channel.invokeMethod('onNoAD');
}

enum NativeADEvent {
  onNoAD,
  onLoadSuccess,
  onClick,
  onExposed,
  onRenderFail,
  onRenderSuccess,
  onAdClosed,
  onVideoCache,
  onVideoStart,
  onVideoResume,
  onVideoPause,
  onVideoComplete,
  onVideoError
}

typedef NativeADEventCallback = Function(NativeADEvent event, Map arguments);
